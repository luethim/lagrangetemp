# evaluate the analytical solution
# created with create_input_analyt.py

import numpy as np
import pylab as plt

import feval.FEval
from feval.fecodes.exodusII.ExodusIIFile import ExodusIIFile

datapath = '../temp2dmoving_analyt'

# # calculate heat conductivity
# def Cp(T):
#     """ heat capacity, Paterson (1994)
#     in units of J kg^{-1} K^{-1}"""
#     return 152.5 + 7.122*(273.15+T)

# def k(T):
#     """ thermal conductivity
#     in units W m^{-1}K^{-1}"""
#     return 9.828 * np.exp((-5.7e-3)*(273.15+T))


# def kappa(T):
#     return k(T)/(900.*Cp(T))

kappa = 1.1e-6
def tempprofile(h, t, T_0, DT_0, omega):
    sok = -h*np.sqrt(omega/(2.*kappa))
    return T_0 + DT_0 *np.exp(sok) * np.sin(omega*t +sok)


secperyear = 365.*24.*3600.
period = 100*secperyear
omega = 2.*np.pi/period

Hs = np.arange(0,600,5)

for step in range(1910,1999,10):
    m  = feval.FEval.FEModel()
    gf = ExodusIIFile(m)
    gf.readFile(datapath+'/out.%04d.e'%step)

    data = []
    for node in m.getNodeNames():
        co = m.Coord[node]
        if -1 < co[0] < 1:
            data.append((co[1], m.NodVar['T'][node]))
    data = np.array(data)
    idx = np.argsort(data[:,0])
    data = data[idx]
    plt.plot(data[:,1], data[:,0], 'ko-')

    Ts = tempprofile(Hs, step*secperyear, -20., -10., omega)

    plt.plot(Ts, 1670.-Hs-1., 'r.-')

plt.ylim(1400, 1680)
plt.show()