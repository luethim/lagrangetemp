# Create the input for the temperature model runs

import numpy as np
import pylab as plt
import scipy.interpolate

# runname
runname = 'no_moulin3d'

# model time step
nsteps = 21
dt = 1.

# initial model geometry
nx = 10
ny = 10
nz = 30
Dx0 = 300.
Dy0 = 300.
Dz0 = 2000.

# heat production steps 
psteps = [
    # (p_smin, p_smax, p_xmin, p_xmax, p_ymin, p_ymax, p_hmin, p_hmax, p_rate)
      # [    10,     15,   0.,   10.,   0.,    10.,   1700.,  0., 0.5],
    # [    70,    80,   -100.,   100.,   -Dy0,    Dy0,   250.,  50., 0.05],
    # [    550,    580,   -100.,   100.,   -Dy0,    Dy0,   500.,  400., 0.5],
    # [    750,    770,   -100.,   100.,   -Dy0,    Dy0,   250.,  50., 0.05],
]
# moulins (vertical temperature at 0 degreees)
msteps = [
    # (t_smin, t_smax, t_x, t_y)
    [    10,     15,   5.,   5.],
]

# mesh refinement
max_h_level = 1
refine_fraction = 0.7
coarsen_fraction = 0.3
dhu = 500.;  # mesh refinement toleranz nach unten
dho = 50.;   # mesh refinement toleranz nach oben

# model domain
L0 = Dx0
H0 = 1670.   # this is Hs[0] from below at 450 km

# HWT bei GULL, dissipation
HWToGULL = 550.
HWTuGULL = 680.
HWTuGULL = 705.
HWToF450 = 1670.-HWToGULL * 1670./705.
HWTuF450 = 1670.-HWTuGULL * 1670./705.
EnhFact  = 5.

# starting position
x0 = 450.
x1 = 530.

xSC   = 498.
xGULL = 517.3
xFOXX = 524.5

## calculate heat conductivity
# def Cp(T):
#     """ heat capacity, Paterson (1994)
#     in units of J kg^{-1} K^{-1}"""
#     return 152.5 + 7.122*(273.15+T)

# def k(T):
#     """ thermal conductivity
#     in units W m^{-1}K^{-1}"""
#     return 9.828 * np.exp((-5.7e-3)*(273.15+T))

# def kappa(T):
#     return k(T)/(900.*Cp(T))

# Ts = np.arange(-50,0,1)
# plt.plot(Ts, kappa(Ts))
# plt.plot(Ts, 7e-11*Ts**2 - 9.6e-9* Ts + 1.1e-6, 'r')
# plt.show()
# stop

# try:
#     vfunk
# except:
if 1:
    # load all the data
    datapath = '../data/temperature'
    vfunk    = np.load(datapath + '/vfunk.npy')
    surffunk = np.load(datapath + '/surffunk.npy')
    bedfunk  = np.load(datapath + '/bedfunk.npy')
    surfdtu = np.load(datapath + '/surfdtu.npy')
    beddtu  = np.load(datapath + '/beddtu.npy')
    Tfunk    = np.load(datapath + '/Tfunk.npy')
    mbfunk   = np.load(datapath + '/mbfunk.npy')
    mbfunkN  = np.load(datapath + '/mbfunkN.npy')
    #prof450  = np.load(datapath + '/funk450.npy')

    # set surface temperature to zero
    Tfunk = np.vstack( (Tfunk[:4], [460.,0,], [475.,0], [481, -10], [500,-9], [600,-5]))

    vinterp    = scipy.interpolate.interp1d(vfunk[:,0], vfunk[:,1])
    surfinterp = scipy.interpolate.interp1d(surfdtu[:,0], surfdtu[:,1]*1000.)
    bedinterp  = scipy.interpolate.interp1d(beddtu[:,0], beddtu[:,1]*1000.)
    Tinterp    = scipy.interpolate.interp1d(Tfunk[:,0], Tfunk[:,1])
    mbinterp   = scipy.interpolate.interp1d(mbfunkN[:,0], mbfunkN[:,1])
    surfspl    = scipy.interpolate.UnivariateSpline(surfdtu[:,0], surfdtu[:,1]*1000.)
    slopespl   = surfspl.derivative()
    bedspl     = scipy.interpolate.UnivariateSpline(beddtu[:,0], beddtu[:,1]*1000.,s=10)

    print ''
    print 'SC  ', surfinterp(xSC)- bedinterp(xSC)
    print 'GULL', surfinterp(xGULL)- bedinterp(xGULL)
    print 'FOXX', surfinterp(xFOXX)- bedinterp(xFOXX)


# calulate dissipation
# A0 = 75  # [MPa^-3 a^-1]

# xs = np.arange(350, 520, 1)

# plt.plot(surfdtu[:,0], 1000.*surfdtu[:,1], 'ko-')
# plt.plot(xs, surfinterp(xs), 'b')
# plt.plot(xs, surfspl(xs), 'r')
# plt.plot(beddtu[:,0], 1000.*beddtu[:,1], 'ko-')
# plt.plot(xs, bedinterp(xs), 'b')
#plt.plot(xs, bedspl(xs), 'r')

# plt.show()
# stop

#xxs   = 0.5*(xs[1:]+xs[:-1])
# slopes = np.absolute(np.diff(surfspl(xs))/1000.)
# plt.plot(xxs, np.rad2deg(np.arctan(slopes)))
# slopes = np.absolute(slopespl(xxs)/1000.)
# plt.plot(xxs, np.rad2deg(np.arctan(slopes)))
# plt.show()
# stop
# for x in xs[::10]:
#     Hs = np.arange(int(surfspl(x)-bedinterp(x)))
#     slope = slopespl(x)
#     tauxz = (900.*9.825*slope*1e-6)*Hs
#     # dissipative heat production rate in units of
#     # MPa^-3 a^-1 MPa^4 = MPa a^-1 = 1e6 Pa/(3.15 e7 s) = 1/31.5 Pa/s = 0.0317 W/m3
#     # P = 2.*A0*tauxz**4 * 0.0317  # w/m3
#     # MPa^-3 a^-1 MPa^4 = MPa a^-1 = 1e6 Pa/a = 1e6 Pa/a = 1e6 W/m3
#     P = 2.*A0*tauxz**4 * 1e6  # W/m3
#     # warming rate
#     plt.plot(P/2098./900., -Hs)
# plt.show()
# stop

# calcualted velocity
# u_s = 2.*A0/4. * (900.*9.825*slopes*1e-6)**3 * (surfspl(xxs)-bedinterp(xxs))**4
# plt.plot(xxs, u_s)
# plt.plot(vfunk[:,0], vfunk[:,1], 'r')
# plt.show()
# stop


# time integration
x = x0
t = 0
step = 0
data = []
while x < x1:
    data.append((t, x,
                 surfinterp(x),
                 bedinterp(x),
                 abs(slopespl(x)/1000.),   # slope is now km and m mixed
                 Tinterp(x),
                 mbinterp(x),
             ))
    v = vinterp(x)/1000.
    x += v*dt
    t += dt
    step += 1
data = np.array(data)

#np.savetxt('tempmodel_dt%03d_input.dat'%dt, data, fmt='%.3f')


# plt.plot(data[:,1], data[:,2], '.-')
# plt.plot(data[:,1], data[:,3], '.-')

## the DTU data
# flowline = np.loadtxt('../../data/radar/THEflowline.txt', delimiter=',')
# plt.plot(flowline[:,0]/1000.+498., flowline[:,-2], 'b-', lw=2)
# plt.plot(flowline[:,0]/1000.+498., flowline[:,-1], 'k-', lw=2)
#plt.plot(vfunk[:,0], vfunk[:,1])


# x vs thickness
xs     = data[:,1]
mbcum  = data[:,-1].cumsum()*dt
Hs     = data[:,2]-data[:,3]
eps_zz = np.hstack((0, np.diff(Hs)/Hs[:-1]))
# soo ists korrekt!
eps_zz_mb = np.hstack((0, (np.diff(Hs) - data[:-1,-1]*dt)/Hs[:-1]))


# plt.plot(xs, eps_zz, 'b')
# plt.plot(xs, eps_zz_mb, 'r')
# plt.show()
# stop

# calculate the lengths
Ls = [L0]
L = L0
for ezz in eps_zz_mb:
    L += L*(-ezz)
    Ls.append(L)
Ls = np.array(Ls)

# plt.plot(xs, Hs, 'k')
# plt.plot(xs, Ls, 'r')
# plt.plot(xs, data[:,3], 'b')
# plt.plot(xs, data[:,4], 'r')

# plt.plot([xSC]*2, [0,1000])
# plt.plot([xGULL]*2, [0,1000])
# plt.plot([xFOXX]*2, [0,1000])


Heff  = Hs - mbcum

# plt.plot(xs, Hs, 'b')
# plt.plot(xs, Heff, 'r')

# plt.ylim(0, 1700)


nsteps = min(nsteps, data.shape[0])

# create an input file for the temperature model code
of = file('../temp2dmoving/timestepdata.dat', 'w')
of.write('# runname\n%s\n' % runname)
of.write('# nx, ny, nz\n')
of.write('%d  %d  %d\n'% (nx, ny, nz))
of.write('# Dx0, Dy0, Dz0\n')
of.write('%f  %f  %f\n'% (Dx0, Dy0, Dz0))
of.write('# HWTo, HWTu, EnhFact\n')
of.write('%f  %f  %f\n'% (HWToF450, HWTuF450, EnhFact))
of.write('#  max_h_level, dhu, dho, refine_fraction, coarsen_fraction\n')
of.write('%d  %f  %f  %f  %f\n' %(max_h_level, dhu, dho, refine_fraction, coarsen_fraction))
of.write('# nsteps,  dt, psteps, msteps\n')
of.write('%d  %f  %d  %d\n'% (nsteps, dt, len(psteps), len(msteps)))
if len(psteps) > 0:
    of.write('# p_smin, p_smax, p_xmin, p_xmax, p_ymin, p_ymax, p_hmin, p_hmax, p_rate\n')
for pstep in psteps:    
    of.write('%d  %d %f  %f  %f  %f  %f  %f  %f\n'% tuple(pstep))
if len(msteps) > 0:
    of.write('# m_smin, m_smax, m_x, m_y\n')
for mstep in msteps:    
    of.write('%d  %d %f  %f\n'% tuple(mstep))
of.write('# t, x, H, slope, Tsurf, bsurf, eps_zz\n')

for d, e in zip(data, eps_zz_mb):
    dd = (d[0], d[1], d[2]-d[3], d[4], d[5], d[6], e)
    of.write('%f %f %f %f %f %f %.8f\n' %dd)

of.close()

# copy the same input file to 3d model
import os
cmd = 'cp ../temp2dmoving/timestepdata.dat ../temp3dmoving/timestepdata.dat'
os.system(cmd)


# data = np.loadtxt('../temp2dmoving/timestepdata.dat', skiprows=13)
# plt.plot(data[:,1], data[:,4])
# plt.show()

# epszz = data[:,-1]
# mbs   = data[:,-2]
# Hsc = []
# H = 1670.
# for eps, mb in zip(epszz, mbs):
#     H = H*(1.+eps)#+mb
#     Hsc.append(H)
# # plt.plot(Hsc)
# # plt.plot(Hs)
# plt.plot(Hs-Hsc)
# plt.show()