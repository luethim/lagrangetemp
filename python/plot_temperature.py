# Temperatur Daten lesen & nett zur Verfuegung stellen

import numpy as np
import pylab as plt
from PIL import Image
import mpl_toolkits.axisartist as AA
from mpl_toolkits.axes_grid1 import host_subplot

#plottype = 'rel'
plottype = 'data'
plottype = 'abs'
#plottype = 'diff'

plotprofiles = ['F450', 'TD5', 'GULL', 'FOXX1', 'FOXX2']

#modelresults = ['dissipation_cuffey']
# modelresults = ['zerosurf_E6', 'E6', 'dissipation_cuffey']                                             
#modelresults = ['dissipation_cuffey', 'E6']
modelresults = ['zerosurf_seven']
linestyles   = ['--', '-', ':']
markers      = ['o', '', '']
modelstep = dict(F450=1, TD5=526, GULL=753, FOXX1=841, FOXX2=841)

funkimage = False

datapath = '../data/temperature'

bottom = dict(
    foxx2=625.,
    gull1=625.,
    THgull2=625.,
    THgull1=625.,
    THfoxx2=625.,
    THfoxx1=625.,
    duck=834.,
    TD1=1.,
    TD2=1.,
    TD3=1.,
    TD4=1.,
    TD5=1257.,
    hare1=700.,
    hare2=700.,
)
sites = dict(
    foxx2='FOXX1',
    gull1='FOXX1',
    THgull1='FOXX1',
    THfoxx2='FOXX2',
    THfoxx1='FOXX2',
    THgull2='FOXX1',
    hare1='GULL',
    hare2='GULL',
    duck='DUCK',
    TD1=1.,
    TD2=1.,
    TD3=1.,
    TD4=1.,
    TD5='TD5',
)

colors = dict(F450 = 'k',
              TD5  ='orange',
              GULL ='r',
              FOXX1 ='b',
              FOXX2 ='c',)

class Container(object):
    def __init__(self):
        pass

class Tempdata(object):

    def __init__(self):
        self.boreholes = dict()

        filenames = ['TempFoxx.txt', 'TempGull.txt']
        data = dict()
        for filename in filenames:
            for line in file(datapath + '/' + filename):
                if len(line) < 2 or  line.startswith('#'):
                    continue
                w = line.split(',')
                data.setdefault(w[0].strip(), []).append([float(x) for x in w[1:]])
            for holename, vals in data.items():
                borehole = Container()
                borehole.depth, borehole.T = np.array(vals).T
                self.boreholes[holename] = borehole
                borehole.bottom = bottom[holename]


        for name in ['TD1', 'TD2', 'TD3', 'TD4', 'TD5']:
            data = np.loadtxt(datapath+'/%s.dat'%name, delimiter=',')
            borehole = Container()
            borehole.depth, borehole.T = data.T
            borehole.bottom = bottom[name]
            self.boreholes[name] = borehole

        # site D data
        # data = np.loadtxt('../../../jako/data/inklino/temp_depth95.plc', skiprows=11)
        # borehole = Container()
        # borehole.depth = data[:,0]
        # borehole.T = data[:,1]
        # borehole.bottom = bottom['duck']
        # self.boreholes['duck'] = borehole

        self.__dict__.update(self.boreholes)
        self.holenames = self.boreholes.keys()
        
if __name__ == '__main__':
    td = Tempdata()
    ax  = host_subplot(1,1,1, axes_class=AA.Axes)

    # if funkimage:
    #     im = Image.open('../../../jako/data/temp/Funk1994_reltemp.png')
    #     ax.imshow(im, extent=[-32,3.5,-1.2,0.06], aspect='auto')

    profiles = [#['foxx2'],
                ['gull1', 'THgull2','THgull1'],
                #['gull1', 'THgull2','THgull1'],
                ['THfoxx2', 'THfoxx1'],
                ['hare1', 'hare2'],
                # ['duck'],
    #             # ['TD1'],
    #             # ['TD2'],
    #             # ['TD3'],
    #             # ['TD4'],
                ['TD5'],
                ]
    allprofiles = dict()
    for names in profiles:
        tdata = []
        for name in names:
            hole = td.boreholes[name]
            tdata.extend(zip(hole.depth, hole.T))
        tdata = np.array(tdata)
        tdata = tdata[ tdata[:,0].argsort() ]
        site = sites[name]
        allprofiles[site] = (tdata, hole.bottom)

    # artprofile = np.array([
    #     [0, -5],
    #     [-100, -15],
    #     [-200, -18],
    #     [-300, -20],
    #     [-400, -21],
    #     [-500, -22],
    #     [-530, -22.2],
    #     [-600, -20],
    #     [-650, -18],
    #     [-700, -12],
    #     [-800, -0.6],
    #     [-835, -0.65]
    #     ] )
    # # plt.plot(artprofile[:,1], artprofile[:,0]/830., 'r', lw=2)

    prof450 = np.load(datapath+'/funk450.npy')
    prof450[:,0] *= 1670.
    allprofiles['F450'] = (prof450, 1670.)

    Cp = 2093.      # in units of J kg^{-1} K^{-1}"""
    L  = 333.5e3    # J / kg^{-1} 

    import feval.FEval
    from feval.fecodes.exodusII.ExodusIIFile import ExodusIIFile

    # plot the temperature profiles
    for profile in plotprofiles:
        if plottype == 'diff' or plottype == 'data':
            if profile == 'F450':
                continue

        tdata, bottom = allprofiles[profile]
        # plot a label
        ax.plot([-5000, -5000], [-5000,-5001], 'o-', color=colors[profile], label=profile)
        # plot the data
        if plottype == 'rel':
            ax.plot(tdata[:,1], -tdata[:,0]/bottom, 'o-', color=colors[profile])
        if plottype == 'abs' or plottype == 'data':
            ax.plot(tdata[:,1], -tdata[:,0], 'o-', color=colors[profile])
            if profile == 'FOXX2':
                continue
        print profile

        if not plottype == 'data':
            # plot the model results
            step = modelstep[profile]
            for nrun, runname in enumerate(modelresults):
                # load time step data
                filename = '../temp2dmoving/results/%s/timestepdata.dat'%runname
                lines = file(filename).readlines()
                # read the step data
                for iline, line in enumerate(lines):
                    if line.startswith('# nsteps'):
                        nsteps, dt, psteps = lines[iline+1].split()
                        nsteps, psteps = int(nsteps), int(psteps)
                        break
                if psteps > 0:
                    iline += psteps + 1
                iline += 3
                tsdata = np.array([map(float, line.split()) for line in lines[iline:iline+nsteps]])

                # extract data from models
                m  = feval.FEval.FEModel()
                mf = ExodusIIFile(m)
                mf.readFile('../temp2dmoving/results/%s/out.%04d.e' %(runname, step))
                Ts = []
                for node, coord in m.Coord.items():
                    if (-10 < coord[0] < 10): # and (-10 < coord[1] < 10):
                        Ts.append(( coord[1], m.NodVar[node]))
                Ts = np.array(Ts)
                idx = Ts[:,0].argsort()
                Ts = Ts[idx]
                zsurf = tsdata[step,2]
                idx = Ts[:,0] < zsurf-1

                if plottype == 'diff':
                    idx = tdata[:,0] > 10  # Oberflächen-Effekte abschneiden
                    Tinterp = np.interp(zsurf-tdata[idx,0], Ts[:,0], Ts[:,1])
                    dT = Tinterp-tdata[idx,1]
                    ax.plot(-dT, -tdata[idx,0], color=colors[profile], ls=linestyles[nrun], marker=markers[nrun], lw=2)
                elif plottype == 'water':
                    idx = tdata[:,0] > 10  # Oberflächen-Effekte abschneiden
                    Tinterp = np.interp(zsurf-tdata[idx,0], Ts[:,0], Ts[:,1])
                    dT = Tinterp-tdata[idx,1]
                    ax.plot(-dT*900*Cp/L, -tdata[idx,0], 'o-', color=colors[profile], ls=['-', '-.', ':'][nrun])
                elif plottype == 'rel':
                    ax.plot(Ts[idx,1], Ts[idx,0]/zsurf-1., '-', color=colors[profile], ls=['-', '-.', ':'][nrun])
                if plottype == 'abs':
                    ax.plot(Ts[idx,1], -zsurf+Ts[idx,0], '-', color=colors[profile], ls=linestyles[nrun], lw=2)


    if plottype == 'diff':
        ax.plot([0,0], [-750, 50], 'k:')
        ax.set_xlim(-1, 16)
        ax.set_ylim(-750, 50)
        ax.set_xlabel('Temperature difference ($^\circ$C)')
        ax.set_ylabel('Depth (m)')
        ax.legend(loc='lower right', fontsize=12)

        # add water axis on top
        ax2 = ax.twiny()
        new_fixed_axis = ax2.get_grid_helper().new_fixed_axis
        ax.axis["top"] = new_fixed_axis(loc="top", axes=ax2, offset=(0, 0))
        axt = ax2.axis["top"]
        axt.toggle(all=True)
        ax2.set_xlabel('Extra heat supply (MJ m$^{-3}$)')
        xlim = np.array(ax.get_xlim())*Cp*900.*1.e-6
        ax2.set_xlim(xlim)

    if plottype == 'water':
        ax.plot([0,0], [-750, 50], 'k:')
        ax.set_xlim(-5, 90)
        ax.set_ylim(-750, 50)
        ax.set_xlabel('Refreezing water (kg/m$^3$)')
        ax.set_ylabel('Depth (m)')
        ax.legend(loc='lower right', fontsize=12)
    if plottype == 'rel':
        ax.set_xlim(-24, 2)
        ax.set_ylim(-1.1, 0.1)
        ax.set_xlabel('Temperature ($^\circ$C)')
        ax.set_ylabel('Relative depth')
        ax.legend(loc='upper left', fontsize=12)
    elif plottype == 'abs':
        ax.plot([0,-0.0797*9.825*1700*900.*1e-6], [0, -1700], 'k--')
        ax.plot([0,0], [50, -1700], 'k:')
        ax.set_xlim(-25, 2)
        ax.set_ylim(-1700, 100)
        ax.set_xlabel('Temperature ($^\circ$C)')
        ax.set_ylabel('Depth (m)')
        ax.legend(loc='upper left', fontsize=12)
    elif plottype == 'data':
        ax.plot([0,-0.0797*9.825*1700*900.*1e-6], [0, -1700], 'k--')
        ax.plot([0,0], [50, -1700], 'k:')
        ax.plot([-1.1,0], [-623]*2, 'k-', lw=3)
        ax.plot([-1.1,0], [-705]*2, 'k-', lw=3)
        ax.set_xlim(-21, 2)
        ax.set_ylim(-750, 20)
        ax.set_xlabel('Temperature ($^\circ$C)')
        ax.set_ylabel('Depth (m)')
        ax.legend(loc='upper left', fontsize=12)
    plt.savefig('temp_profile_%s.pdf' % (plottype+'__'+'_'.join(modelresults)))
    plt.show()
