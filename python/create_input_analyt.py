# Create the input for the temperature model runs
#
# this is for the analytical solution

import numpy as np
import pylab as plt
import scipy.interpolate

# model time step
nsteps = 2001
dt = 1.
period = 100.
deltaT = 10.

# initial model geometry
nx = 2
ny = 0
nz = 20
Dx0 = 500.
Dy0 = 0.
Dz0 = 2000.

# mesh refinement
max_h_level = 4
refine_fraction = 0.7
coarsen_fraction = 0.3
dhu = 200.;  # mesh refinement toleranz nach unten
dho = 1.;   # mesh refinement toleranz nach oben

# model domain
L0 = Dx0
H0 = 1670.   # this is Hs[0] from below at 450 km

# starting position
x0 = 450.
x1 = 530.

xSC   = 498.
xGULL = 517.3
xFOXX = 524.5

## calculate heat conductivity
# def Cp(T):
#     """ heat capacity, Paterson (1994)
#     in units of J kg^{-1} K^{-1}"""
#     return 152.5 + 7.122*(273.15+T)

# def k(T):
#     """ thermal conductivity
#     in units W m^{-1}K^{-1}"""
#     return 9.828 * np.exp((-5.7e-3)*(273.15+T))

# def kappa(T):
#     return k(T)/(900.*Cp(T))

# Ts = np.arange(-50,0,1)
# plt.plot(Ts, kappa(Ts))
# plt.plot(Ts, 7e-11*Ts**2 - 9.6e-9* Ts + 1.1e-6, 'r')
# plt.show()
# stop


# time integration
x = x0
t = 0
step = 0
data = []
for i in range(nsteps):
    data.append((t, x,
                 1670.,
                 0.,
                 -20.-deltaT*np.sin(2.*np.pi*t/period),
                 0.,
             ))
    t += dt
    step += 1
data = np.array(data)

nsteps = min(nsteps, data.shape[0])

# create an input file for the temperature model code
of = file('../temp2dmoving_analyt/timestepdata.dat', 'w')
of.write('# nx, ny, nz\n')
of.write('%d  %d  %d\n'% (nx, ny, nz))
of.write('# Dx0, Dy0, Dz0\n')
of.write('%f  %f  %f\n'% (Dx0, Dy0, Dz0))
of.write('#  max_h_level, dhu, dho, refine_fraction, coarsen_fraction\n')
of.write('%d  %f  %f  %f  %f\n' %(max_h_level, dhu, dho, refine_fraction, coarsen_fraction))
of.write('# nsteps,  dt\n')
of.write('%d  %f \n'% (nsteps, dt))
of.write('# t, x, H, Tsurf, bsurf, eps_zz\n')

for d in data:
    dd = (d[0], d[1], d[2]-d[3], d[4], d[5], 0.)
    of.write('%f %f %f %f %f %.8f\n' %dd)

of.close()

# data = np.loadtxt('timestepdata.dat', skiprows=9)
# epszz = data[:,-1]
# mbs   = data[:,-2]
# Hsc = []
# H = 1670.
# for eps, mb in zip(epszz, mbs):
#     H = H*(1.+eps)#+mb
#     Hsc.append(H)
# plt.plot(Hsc)
# plt.plot(Hs)

# plt.show()