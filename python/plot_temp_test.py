#  Modell Resultate 2D und 3D miteinander vergleichen

import numpy as np
import pylab as plt

modelresults = ['no_moulin3d','moulin3d']

step = 15

import feval.FEval
from feval.fecodes.exodusII.ExodusIIFile import ExodusIIFile

for nrun, runname in enumerate(modelresults):
    # # load time step data
    # filename = '../temp2dmoving/results/%s/timestepdata.dat'%runname
    # lines = file(filename).readlines()
    # # read the step data
    # for iline, line in enumerate(lines):
    #     if line.startswith('# nsteps'):
    #         nsteps, dt, psteps = lines[iline+1].split()
    #         nsteps, psteps = int(nsteps), int(psteps)
    #         break
    # if psteps > 0:
    #     iline += psteps + 1
    # iline += 3
    # tsdata = np.array([map(float, line.split()) for line in lines[iline:iline+nsteps]])

    # # extract data from models
    # m  = feval.FEval.FEModel()
    # mf = ExodusIIFile(m)
    # mf.readFile('../temp2dmoving/results/%s/out.%04d.e' %(runname, step))
    # x2 = m.boundingbox[0,0]
    # Ts = []
    # for node, coord in m.Coord.items():
    #     if (x2-1. < coord[0] < x2+1): # and (-10 < coord[1] < 10):
    #         Ts.append(( coord[1], m.NodVar[node]))
    # Ts = np.array(Ts)
    # idx = Ts[:,0].argsort()
    # Ts = Ts[idx]
    # # plt.plot(Ts[:,1], Ts[:,0], '-o', color='b')
    # Ts2d = Ts

    m  = feval.FEval.FEModel()
    mf = ExodusIIFile(m)
    mf.readFile('../temp3dmoving/results/%s/out.%04d.e' %(runname, step))
    x0 = 0.
    Ts = []
    for node, coord in m.Coord.items():
        if (x0-10. < coord[0] < x0+10.) and (-10 < coord[1] < 10):
            Ts.append(( coord[2], m.NodVar[node]))
    Ts = np.array(Ts)
    idx = Ts[:,0].argsort()
    Ts = Ts[idx]
    plt.plot(Ts[:,1]+0.1, Ts[:,0], '-o', color='r')
    # Ts3d = Ts

    # plt.plot(Ts2d[:,1]-Ts3d[:,1])

plt.show()